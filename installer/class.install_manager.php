<?php 

class InstallManager {
    protected $api_url = 'https://api.biblesupersearch.com';
    protected $statics;
    protected $errors = [];

    public function __construct() {
        $this->_loadStatics();
    }

    public function checkSettings() {
        if($this->statics['php_error']) {
            $this->error[] = 'Minimum required PHP version is ' . $this->statics['php_required_min'] . '; you are running version ' . self::getPHPVersion();
            $success = FALSE;
        }

        $msg = ' needs to be writeable by the web process';
        $success = TRUE;
        $dot_env_path = $_SERVER['DOCUMENT_ROOT'] . '/../.env';
        $dot_env_example_path = $_SERVER['DOCUMENT_ROOT'] . '/../.env-example';
        $extensions = ['OpenSSL', 'PDO', 'Mbstring', 'Tokenizer', 'XML', 'Zip', 'Ctype', 'JSON', 'BCMath'];

        if(!is_writable($_SERVER['DOCUMENT_ROOT'] . '/..')) {
            $this->errors[] = 'Entire API root directory' . $msg;
            $success = FALSE;
        }        
        
        if(!is_file($dot_env_path)) {
            if(!copy($dot_env_example_path, $dot_env_path)) {
                $this->errors[] = 'Could not create .env';
                $success = FALSE;
            }
        }
        elseif(!is_writable($dot_env_path)) {
            $this->errors[] = '.env' . $msg;
            $success = FALSE;
        }

        if(!is_writable($_SERVER['DOCUMENT_ROOT'] . '/../installer/downloads')) {
            $this->errors[] = 'installer/downloads' . $msg;
            $success = FALSE;
        }

        foreach($extensions as $ext) {
            if(!extension_loaded($ext)) {
                $this->errors[] = 'Extension ' . $ext . ' is required';
                $success = FALSE;
            }
        }

        return $success;
    }

    public function downloadFile() {
        $allow_url_fopen = intval(ini_get('allow_url_fopen'));
        $file_info = $this->getFileName();

        if(!is_file($file_info['path'])) {
            return TRUE;
        }

        if(!$this->statics) {
            return FALSE;
        }

        $sources = [
            'https://sourceforge.net/projects/biblesuper/files/API',
        ];

        foreach($sources as $source) {
            $result = $write = FALSE;
            $url = $source . '/biblesupersearch_api_' . $this->statics['version'] . '.zip';

            $file_name = basename($url);
            $file_path = $_SERVER['DOCUMENT_ROOT'] . '/../installer/downloads/' . $file_name;

            if($allow_url_fopen == 1) {        
                $result  = file_get_contents($url);

                if($result) {
                    $write = file_put_contents($file_path, $result);
                }
            }
            
            if($result === FALSE && function_exists('curl_init')) {  
                $ch = curl_init($url);

                // Open file  
                $fp = fopen($file_path, 'wb'); 

                if($fp) {
                    $write = TRUE;

                    // It set an option for a cURL transfer 
                    curl_setopt($ch, CURLOPT_FILE, $fp); 
                    curl_setopt($ch, CURLOPT_HEADER, 0); 
                      
                    // Perform a cURL session 
                    $result = curl_exec($ch); 
                      
                    // Closes a cURL session and frees all resources 
                    curl_close($ch); 
                      
                    // Close file 
                    fclose($fp); 
                }
            }

            if($result && $write) {
                return TRUE;
            }

            if(!$result) {
                $this->errors[] = 'Could not load file from ' . $url;
            }
            elseif(!$write) {
                $this->errors[] = 'Could not write to ' . $file_path;
            }
            
        }

        return FALSE;
    }

    public function extractFile() {
        if(!$this->statics) {
            return FALSE;
        }

        $file = $this->getFileName();

        $extract_path = $_SERVER['DOCUMENT_ROOT'] . '/..';

        if(!is_file($file['path'])) {
            $this->errors[] = 'API file does not exist: ' . $file['name'];
            return FALSE;
        }

        $Zip  = new ZipArchive();
        $res  = $Zip->open($file['path']);

        if($res === TRUE) {
            $success = $Zip->extractTo($extract_path);
            $Zip->close();

            if(!$success) {
                $this->errors[] = 'Could not extract download file';
            }

            return $success;
        }

        return FALSE;
    }

    public function getFileName() {
        $file_name = 'biblesupersearch_api_' . $this->statics['version'] . '.zip';
        $file_path = $_SERVER['DOCUMENT_ROOT'] . '/../installer/downloads/' . $file_name;

        return [
            'name' => $file_name,
            'path' => $file_path,
        ];
    }

    public function getStatics() {
        return $this->statics;
    }    

    public function getErrors() {
        return $this->errors;
    }

    public static function getPHPVersion() {
        $ver = explode('.', PHP_VERSION);
        $php_version = (int) $ver[0] . '.' . (int) $ver[1] . '.' . (int) $ver[2];
        return $php_version;
    }

    protected function _loadStatics() {
        $data = array('pher' => self::getPHPVersion());

        $results = $this->_apiActionHelper('version', $this->api_url, $data);

        if(!$results) {
            $this->statics = FALSE;
        }
        else {
            $this->statics = $results['results'];
        }
    }

    protected function _apiActionHelper($action, $api_url, $data) {
        $url_action = ($action == 'query') ? '/api' : '/api/' . $action;
        $url = $api_url . $url_action;

        $result = FALSE;
        $allow_url_fopen = intval(ini_get('allow_url_fopen'));
        $err = error_reporting();
        error_reporting(E_ERROR | E_PARSE);

        // Attempt 1: Via file_get_contents
        if($allow_url_fopen == 1) {        
            $options = array(
                'http' => array(        // Use key 'http' even if you send the request to https://
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                )
            );
 
            $context = stream_context_create($options);
            $result  = file_get_contents($url, FALSE, $context);
        }
        
        // Attempt 2: Fall back to cURL
        if($result === FALSE && function_exists('curl_init')) {        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            $result = curl_exec($ch);
            curl_close($ch);
        }

        if ($result === FALSE) { 
            if(!function_exists('curl_init') && $allow_url_fopen == 0) {
                $this->errors[] = 'Error: please have your web host turn on php.ini config allow_url_fopen OR install cURL to continue';
            }
            else {
                $this->errors[] = 'Error: unable to load version data from the Bible SuperSearch API server at ' . $url;
            }
        }

        error_reporting($err);
        return ($result === FALSE) ? FALSE : json_decode($result, TRUE);
    }
}