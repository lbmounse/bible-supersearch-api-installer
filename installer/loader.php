<?php


if(!isset($errors) || !is_array($errors)) {
    $errors = [];
}

$blocking_errors = FALSE;
$script = array_key_exists('process', $_POST) ? $_POST['process'] : NULL;

$script_file = dirname(__FILE__) . '/scripts/' . $script . '.php';

if(!is_file($script_file)) {
    $script_file = dirname(__FILE__) . '/scripts/initial.php';
}

require_once(dirname(__FILE__) . '/class.install_manager.php');

$view = NULL;

$Installer = new InstallManager();

// var_dump($Installer->getStatics());
// die();

if(!$Installer->getStatics()) {
    var_dump($Installer->getErrors());
}

if($Installer->checkSettings()) {
    // var_dump($Installer->getErrors());

    // $Installer->downloadFile();

}
else {
    $errors = $Installer->getErrors();
    $blocking_errors = TRUE;
}


require_once($script_file);

if($view) {
    $view = dirname(__FILE__) . '/views/' . $view . '.php';

    if(!is_file($view)) {
        $view = NULL;
    }
}
else {
    $view = NULL;
    // $view = dirname(__FILE__) . '/views/' . $view . '.php';
}

function sannitize($fields) {
    $san = array();

    foreach($fields as $field) {
        if(!array_key_exists($field, $_POST)) {
            $san[$field] = NULL;
            continue;
        }

        $value = $_POST[$field];
        $value = trim($value);
        $san[$field] = $value;
    }

    return $san;
}

function displayErrors($errors) {
    ?>
    <div id='errors'>
        <h2>Some errors have occurred</h2>
        <ul class='errors'>
            <?php foreach ($errors as $error) : ?>
                <li><?php echo $error ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php
}
