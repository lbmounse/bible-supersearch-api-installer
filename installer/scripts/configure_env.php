<?php
    $fields = ['db_host', 'db_database', 'db_username', 'db_password', 'db_prefix'];

    $sanitized = sannitize($fields);

    if(!$sanitized['db_host']) {
        $sanitized['db_host'] = 'localhost';
    }    

    if(!$sanitized['db_prefix']) {
        $sanitized['db_prefix'] = 'bss_';
    }

    if(!$sanitized['db_database']) {
        $errors[] = 'Database name is required';
    }    

    if(!$sanitized['db_username']) {
        $errors[] = 'User name is required';
    }    

    if(!$sanitized['db_password']) {
        $errors[] = 'Password is required';
    }

    if(!$errors) {    
        try {
            $PDO = new PDO("mysql:host={$sanitized['db_host']};dbname=" . $sanitized['db_database'], $sanitized['db_username'], $sanitized['db_password']);
        }
        catch(Exception $e) {
            $errors[] = 'Could not connect with the provided credentials';
        }
    }

    if(!$errors) {
        $dot_env_path = $_SERVER['DOCUMENT_ROOT'] . '/../.env';
        $dot_env_example_path = $_SERVER['DOCUMENT_ROOT'] . '/../.env-example';
        $env = file_get_contents($dot_env_example_path);

        if($env) {
            foreach($fields as $field) {
                $env = str_replace('[' . $field . ']', $sanitized[$field], $env);
            }

            if(!file_put_contents($dot_env_path, $env)) {
                $errors[] = 'Could not update .env';
            }
        }
        else {
            $errors[] = 'Could not open .env';
        }
    }

    if($errors) {
        $view = 'db_setup';
    }
    else {
        // $view = 'download';
        require_once(dirname(__FILE__) . '/download.php');
    }