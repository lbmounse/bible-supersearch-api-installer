<?php

$success = FALSE;

if($Installer->downloadFile()) {
    // do something
    $view = 'download';

    if($Installer->extractFile()) {
        $success = TRUE;
    }
}

if($success) {
    header('Location: /'); // Hand off to installer within Bible SuperSearch API
    exit;
}
else {
    $errors = $Installer->getErrors();
    $view = 'errors';
}


