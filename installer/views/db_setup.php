<form action='index.php' method='POST' onsubmit='submitForm();' id='form'>
    <input type='hidden' name='process' value='configure_env' />

    <table>
        <tr><th colspan='2'>Database Connection</th></tr>
        <tr>
            <td>Hostname</td>
            <td><input name='db_host' placeholder="localhost" value='<?php echo $sanitized['db_host'] ?>' /></td>
        </tr>        
        <tr>
            <td>Database Name</td>
            <td><input name='db_database' value='<?php echo $sanitized['db_database'] ?>' /></td>
        </tr>        
        <tr>
            <td>User Name</td>
            <td><input name='db_username' value='<?php echo $sanitized['db_username'] ?>' /></td>
        </tr>        
        <tr>
            <td>Password</td>
            <td><input name='db_password' value='<?php echo $sanitized['db_password'] ?>' /></td>
        </tr>        
        <tr>
            <td>Prefix</td>
            <td><input name='db_prefix' placeholder='bss_' value='<?php echo $sanitized['db_prefix'] ?>' /></td>
        </tr>
    </table>
    <input type='submit' value='Save and Continue' <?php if ($blocking_errors): ?> disabled='disabled' <?php endif; ?> >

</form>

<div style='display:none' class='downloading' id='downloading'>
    Downloading, please wait ...
</div>

<script>

function submitForm() {
    var to = window.setTimeout(function() {
        var form = document.getElementById('form');
        var errors = document.getElementById('errors');
        var load = document.getElementById('downloading');

        if(errors) {
            errors.style.display = 'none';
        }
        
        form.style.display = 'none';
        load.style.display = 'block';
    }, 500);
}

</script>
