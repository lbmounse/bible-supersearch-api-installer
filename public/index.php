<?php
    ini_set('display_errors', 1);

    require_once( $_SERVER['DOCUMENT_ROOT'] . '/../installer/loader.php');
?>

<html>
    <head>
        <title>Bible SuperSearch API Installer</title>

        <style>
            <?php echo file_get_contents( $_SERVER['DOCUMENT_ROOT'] . '/../installer/style.css'); ?>
        </style>
    </head>
    <body>
        <div id='app'>
            <div id='container'>
                <div id='content'>
                    <div id='main_body'>
                        <?php if($errors) displayErrors($errors) ?>

                        <?php if($view) require_once($view) ?>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>